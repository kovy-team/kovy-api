let Hapi = require('@hapi/hapi')
let mongoose = require('mongoose')
let RestHapi = require('rest-hapi')

async function api() {
  try {

    let server = Hapi.Server({
      port: process.env.PORT || 8080,
      routes: {
        validate: {
          failAction: async (request, h, err) => {
            RestHapi.logger.error(err);
            throw err;
          }
        }
      }
    })

    let config = {
      appTitle: 'Handmade shop online api',
      enableTextSearch: true,
      logRoutes: true,
      docExpansion: 'list',
      swaggerHost: process.env.DOMAIN || 'localhost:8080',
      mongo: {
        URI: process.env.DB_SERVER_URL || 'mongodb://localhost:27017/beauty-hand-letter',
      }
    }

    config.version = '1.0.0'
    config.modelPath = 'models'
    config.apiPath = 'api'

    config.enableCreatedAt = true
    config.enableUpdatedAt = true
    config.enableDeletedAt = true
    config.enableCreatedBy = false
    config.enableUpdatedBy = false
    config.enableDeletedBy = false

    config.enableAuditLog = true
    config.enableDocumentScopes = true

    config.userIdKey = 'user._id'

    config.enableQueryValidation = true
    config.enablePayloadValidation = true
    config.enableResponseValidation = true

    config.enableSwaggerHttps = true

    await server.register({
      plugin: RestHapi,
      options: {
        mongoose: mongoose,
        config: config,
      },
    })

    await server.start()

    RestHapi.logUtil.logActionComplete(RestHapi.logger, 'Server Initialized', server.info)

    return server
  } catch (err) {
    console.log('Error starting server:', err)
  }
}

module.exports = api()
